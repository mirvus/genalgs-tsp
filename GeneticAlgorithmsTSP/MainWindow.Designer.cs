﻿namespace GeneticAlgorithmsTSP
{
    partial class MainWindowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title11 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title12 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title13 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title14 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea15 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Title title15 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.BestTourMap = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.WorstTourMap = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.AvgAdaptationChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.MinAdaptationChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.MaxAdaptationChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelMaxGen = new System.Windows.Forms.Label();
            this.buttonRunSimulation = new System.Windows.Forms.Button();
            this.numericUpDownPopCount = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCrossProb = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMutationProb = new System.Windows.Forms.NumericUpDown();
            this.labelPopCount = new System.Windows.Forms.Label();
            this.labelCrossProb = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownMaxGenerations = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBoxFitnessCharts = new System.Windows.Forms.GroupBox();
            this.groupBoxTour = new System.Windows.Forms.GroupBox();
            this.groupBoxFileSelection = new System.Windows.Forms.GroupBox();
            this.buttonBrowseFile = new System.Windows.Forms.Button();
            this.textBoxFileSelect = new System.Windows.Forms.TextBox();
            this.labelFileChooser = new System.Windows.Forms.Label();
            this.groupBoxExecLog = new System.Windows.Forms.GroupBox();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.BestTourMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorstTourMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AvgAdaptationChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinAdaptationChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxAdaptationChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPopCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCrossProb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMutationProb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxGenerations)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBoxFitnessCharts.SuspendLayout();
            this.groupBoxTour.SuspendLayout();
            this.groupBoxFileSelection.SuspendLayout();
            this.groupBoxExecLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // BestTourMap
            // 
            chartArea11.Name = "ChartArea1";
            this.BestTourMap.ChartAreas.Add(chartArea11);
            this.BestTourMap.Location = new System.Drawing.Point(15, 19);
            this.BestTourMap.Name = "BestTourMap";
            this.BestTourMap.Size = new System.Drawing.Size(360, 360);
            this.BestTourMap.TabIndex = 0;
            this.BestTourMap.Text = "citiesMap";
            title11.Name = "CitiesMapTitle";
            this.BestTourMap.Titles.Add(title11);
            // 
            // WorstTourMap
            // 
            chartArea12.Name = "ChartArea1";
            this.WorstTourMap.ChartAreas.Add(chartArea12);
            this.WorstTourMap.Location = new System.Drawing.Point(381, 19);
            this.WorstTourMap.Name = "WorstTourMap";
            this.WorstTourMap.Size = new System.Drawing.Size(360, 360);
            this.WorstTourMap.TabIndex = 1;
            this.WorstTourMap.Text = "citiesMap";
            title12.Name = "CitiesMapTitle";
            this.WorstTourMap.Titles.Add(title12);
            // 
            // AvgAdaptationChart
            // 
            chartArea13.Name = "ChartArea1";
            this.AvgAdaptationChart.ChartAreas.Add(chartArea13);
            this.AvgAdaptationChart.Location = new System.Drawing.Point(9, 19);
            this.AvgAdaptationChart.Name = "AvgAdaptationChart";
            this.AvgAdaptationChart.Size = new System.Drawing.Size(320, 280);
            this.AvgAdaptationChart.TabIndex = 2;
            this.AvgAdaptationChart.Text = "chart1";
            title13.Name = "Title1";
            this.AvgAdaptationChart.Titles.Add(title13);
            // 
            // MinAdaptationChart
            // 
            chartArea14.Name = "ChartArea1";
            this.MinAdaptationChart.ChartAreas.Add(chartArea14);
            this.MinAdaptationChart.Location = new System.Drawing.Point(335, 19);
            this.MinAdaptationChart.Name = "MinAdaptationChart";
            this.MinAdaptationChart.Size = new System.Drawing.Size(320, 280);
            this.MinAdaptationChart.TabIndex = 3;
            this.MinAdaptationChart.Text = "chart1";
            title14.Name = "Title1";
            this.MinAdaptationChart.Titles.Add(title14);
            // 
            // MaxAdaptationChart
            // 
            chartArea15.Name = "ChartArea1";
            this.MaxAdaptationChart.ChartAreas.Add(chartArea15);
            this.MaxAdaptationChart.Location = new System.Drawing.Point(661, 19);
            this.MaxAdaptationChart.Name = "MaxAdaptationChart";
            this.MaxAdaptationChart.Size = new System.Drawing.Size(320, 280);
            this.MaxAdaptationChart.TabIndex = 4;
            this.MaxAdaptationChart.Text = "chart1";
            title15.Name = "Title1";
            this.MaxAdaptationChart.Titles.Add(title15);
            // 
            // labelMaxGen
            // 
            this.labelMaxGen.AutoSize = true;
            this.labelMaxGen.Location = new System.Drawing.Point(6, 16);
            this.labelMaxGen.Name = "labelMaxGen";
            this.labelMaxGen.Size = new System.Drawing.Size(64, 13);
            this.labelMaxGen.TabIndex = 7;
            this.labelMaxGen.Text = "Generations";
            // 
            // buttonRunSimulation
            // 
            this.buttonRunSimulation.Location = new System.Drawing.Point(79, 118);
            this.buttonRunSimulation.Name = "buttonRunSimulation";
            this.buttonRunSimulation.Size = new System.Drawing.Size(75, 23);
            this.buttonRunSimulation.TabIndex = 9;
            this.buttonRunSimulation.Text = "Start";
            this.buttonRunSimulation.UseVisualStyleBackColor = true;
            this.buttonRunSimulation.Click += new System.EventHandler(this.buttonRunSimulation_Click);
            // 
            // numericUpDownPopCount
            // 
            this.numericUpDownPopCount.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownPopCount.Location = new System.Drawing.Point(123, 40);
            this.numericUpDownPopCount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownPopCount.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownPopCount.Name = "numericUpDownPopCount";
            this.numericUpDownPopCount.Size = new System.Drawing.Size(100, 20);
            this.numericUpDownPopCount.TabIndex = 10;
            this.numericUpDownPopCount.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // numericUpDownCrossProb
            // 
            this.numericUpDownCrossProb.DecimalPlaces = 3;
            this.numericUpDownCrossProb.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numericUpDownCrossProb.Location = new System.Drawing.Point(123, 66);
            this.numericUpDownCrossProb.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCrossProb.Name = "numericUpDownCrossProb";
            this.numericUpDownCrossProb.Size = new System.Drawing.Size(100, 20);
            this.numericUpDownCrossProb.TabIndex = 11;
            this.numericUpDownCrossProb.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            // 
            // numericUpDownMutationProb
            // 
            this.numericUpDownMutationProb.DecimalPlaces = 3;
            this.numericUpDownMutationProb.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDownMutationProb.Location = new System.Drawing.Point(123, 92);
            this.numericUpDownMutationProb.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMutationProb.Name = "numericUpDownMutationProb";
            this.numericUpDownMutationProb.Size = new System.Drawing.Size(100, 20);
            this.numericUpDownMutationProb.TabIndex = 12;
            this.numericUpDownMutationProb.Value = new decimal(new int[] {
            3,
            0,
            0,
            196608});
            // 
            // labelPopCount
            // 
            this.labelPopCount.AutoSize = true;
            this.labelPopCount.Location = new System.Drawing.Point(6, 42);
            this.labelPopCount.Name = "labelPopCount";
            this.labelPopCount.Size = new System.Drawing.Size(88, 13);
            this.labelPopCount.TabIndex = 13;
            this.labelPopCount.Text = "Population Count";
            // 
            // labelCrossProb
            // 
            this.labelCrossProb.AutoSize = true;
            this.labelCrossProb.Location = new System.Drawing.Point(6, 68);
            this.labelCrossProb.Name = "labelCrossProb";
            this.labelCrossProb.Size = new System.Drawing.Size(103, 13);
            this.labelCrossProb.TabIndex = 14;
            this.labelCrossProb.Text = "Crossover Probabilty";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Mutation Probability";
            // 
            // numericUpDownMaxGenerations
            // 
            this.numericUpDownMaxGenerations.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMaxGenerations.Location = new System.Drawing.Point(123, 14);
            this.numericUpDownMaxGenerations.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownMaxGenerations.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownMaxGenerations.Name = "numericUpDownMaxGenerations";
            this.numericUpDownMaxGenerations.Size = new System.Drawing.Size(100, 20);
            this.numericUpDownMaxGenerations.TabIndex = 16;
            this.numericUpDownMaxGenerations.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelMaxGen);
            this.groupBox1.Controls.Add(this.buttonRunSimulation);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numericUpDownMaxGenerations);
            this.groupBox1.Controls.Add(this.labelCrossProb);
            this.groupBox1.Controls.Add(this.numericUpDownPopCount);
            this.groupBox1.Controls.Add(this.labelPopCount);
            this.groupBox1.Controls.Add(this.numericUpDownCrossProb);
            this.groupBox1.Controls.Add(this.numericUpDownMutationProb);
            this.groupBox1.Location = new System.Drawing.Point(12, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(234, 149);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameters";
            // 
            // groupBoxFitnessCharts
            // 
            this.groupBoxFitnessCharts.Controls.Add(this.AvgAdaptationChart);
            this.groupBoxFitnessCharts.Controls.Add(this.MinAdaptationChart);
            this.groupBoxFitnessCharts.Controls.Add(this.MaxAdaptationChart);
            this.groupBoxFitnessCharts.Location = new System.Drawing.Point(12, 407);
            this.groupBoxFitnessCharts.Name = "groupBoxFitnessCharts";
            this.groupBoxFitnessCharts.Size = new System.Drawing.Size(990, 317);
            this.groupBoxFitnessCharts.TabIndex = 18;
            this.groupBoxFitnessCharts.TabStop = false;
            this.groupBoxFitnessCharts.Text = "Fitness Scores";
            // 
            // groupBoxTour
            // 
            this.groupBoxTour.Controls.Add(this.BestTourMap);
            this.groupBoxTour.Controls.Add(this.WorstTourMap);
            this.groupBoxTour.Location = new System.Drawing.Point(252, 12);
            this.groupBoxTour.Name = "groupBoxTour";
            this.groupBoxTour.Size = new System.Drawing.Size(750, 389);
            this.groupBoxTour.TabIndex = 19;
            this.groupBoxTour.TabStop = false;
            this.groupBoxTour.Text = "Best and Worst Tour in Last Generation";
            // 
            // groupBoxFileSelection
            // 
            this.groupBoxFileSelection.Controls.Add(this.buttonBrowseFile);
            this.groupBoxFileSelection.Controls.Add(this.textBoxFileSelect);
            this.groupBoxFileSelection.Controls.Add(this.labelFileChooser);
            this.groupBoxFileSelection.Location = new System.Drawing.Point(12, 12);
            this.groupBoxFileSelection.Name = "groupBoxFileSelection";
            this.groupBoxFileSelection.Size = new System.Drawing.Size(234, 49);
            this.groupBoxFileSelection.TabIndex = 20;
            this.groupBoxFileSelection.TabStop = false;
            this.groupBoxFileSelection.Text = "File selection";
            // 
            // buttonBrowseFile
            // 
            this.buttonBrowseFile.Location = new System.Drawing.Point(153, 15);
            this.buttonBrowseFile.Name = "buttonBrowseFile";
            this.buttonBrowseFile.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowseFile.TabIndex = 2;
            this.buttonBrowseFile.Text = "Browse";
            this.buttonBrowseFile.UseVisualStyleBackColor = true;
            this.buttonBrowseFile.Click += new System.EventHandler(this.buttonBrowseFile_Click);
            // 
            // textBoxFileSelect
            // 
            this.textBoxFileSelect.Location = new System.Drawing.Point(38, 17);
            this.textBoxFileSelect.Name = "textBoxFileSelect";
            this.textBoxFileSelect.ReadOnly = true;
            this.textBoxFileSelect.Size = new System.Drawing.Size(110, 20);
            this.textBoxFileSelect.TabIndex = 1;
            // 
            // labelFileChooser
            // 
            this.labelFileChooser.AutoSize = true;
            this.labelFileChooser.Location = new System.Drawing.Point(6, 20);
            this.labelFileChooser.Name = "labelFileChooser";
            this.labelFileChooser.Size = new System.Drawing.Size(26, 13);
            this.labelFileChooser.TabIndex = 0;
            this.labelFileChooser.Text = "File:";
            // 
            // groupBoxExecLog
            // 
            this.groupBoxExecLog.Controls.Add(this.listBoxLog);
            this.groupBoxExecLog.Location = new System.Drawing.Point(12, 222);
            this.groupBoxExecLog.Name = "groupBoxExecLog";
            this.groupBoxExecLog.Size = new System.Drawing.Size(234, 179);
            this.groupBoxExecLog.TabIndex = 21;
            this.groupBoxExecLog.TabStop = false;
            // 
            // listBoxLog
            // 
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(9, 19);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.ScrollAlwaysVisible = true;
            this.listBoxLog.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBoxLog.Size = new System.Drawing.Size(214, 147);
            this.listBoxLog.TabIndex = 0;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.Filter = "CSV files|*.csv";
            // 
            // MainWindowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 736);
            this.Controls.Add(this.groupBoxExecLog);
            this.Controls.Add(this.groupBoxFileSelection);
            this.Controls.Add(this.groupBoxTour);
            this.Controls.Add(this.groupBoxFitnessCharts);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainWindowForm";
            this.Text = "Travelling Salesman problem - Genetic algorithms";
            ((System.ComponentModel.ISupportInitialize)(this.BestTourMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorstTourMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AvgAdaptationChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinAdaptationChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxAdaptationChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPopCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCrossProb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMutationProb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxGenerations)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxFitnessCharts.ResumeLayout(false);
            this.groupBoxTour.ResumeLayout(false);
            this.groupBoxFileSelection.ResumeLayout(false);
            this.groupBoxFileSelection.PerformLayout();
            this.groupBoxExecLog.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart BestTourMap;
        private System.Windows.Forms.DataVisualization.Charting.Chart WorstTourMap;
        private System.Windows.Forms.DataVisualization.Charting.Chart AvgAdaptationChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart MinAdaptationChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart MaxAdaptationChart;
        private System.Windows.Forms.Label labelMaxGen;
        private System.Windows.Forms.Button buttonRunSimulation;
        private System.Windows.Forms.NumericUpDown numericUpDownPopCount;
        private System.Windows.Forms.NumericUpDown numericUpDownCrossProb;
        private System.Windows.Forms.NumericUpDown numericUpDownMutationProb;
        private System.Windows.Forms.Label labelPopCount;
        private System.Windows.Forms.Label labelCrossProb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxGenerations;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxFitnessCharts;
        private System.Windows.Forms.GroupBox groupBoxTour;
        private System.Windows.Forms.GroupBox groupBoxFileSelection;
        private System.Windows.Forms.Button buttonBrowseFile;
        private System.Windows.Forms.TextBox textBoxFileSelect;
        private System.Windows.Forms.Label labelFileChooser;
        private System.Windows.Forms.GroupBox groupBoxExecLog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ListBox listBoxLog;
    }
}