﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmsTSP
{
    public class City
    {
        static int nextId = 1;
        public int id { get; private set; }
        public string name { get; private set; }
        public int x { get; private set; }
        public int y { get; private set; }

        public City(string name, int x, int y)
        {
            this.id = nextId++;
            this.name = name;
            this.x = x;
            this.y = y;
        }

        public double distanceToCity(City city)
        {
            int xDist = Math.Abs(this.x - city.x);
            int yDist = Math.Abs(this.y - city.y);
            return Math.Sqrt((xDist * xDist) + (yDist * yDist));
        }

        public override string ToString()
        {
            return this.name + "(" + this.id + "): (" + this.x.ToString() + ", " + this.y.ToString() + ")";
        }
    }
}
