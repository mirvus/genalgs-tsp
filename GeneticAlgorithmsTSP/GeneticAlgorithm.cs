﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmsTSP
{
    class GeneticAlgorithm
    {
        public Population pop;
        private int popSize;
        private double pcross;
        private double pmutation;
        private Random rnd;

        public GeneticAlgorithm(Tour tour, int popSize, double pcross, double pmutation)
        {
            this.pop = new Population(tour, popSize);
            this.popSize = popSize;
            this.pcross = pcross;
            this.pmutation = pmutation;
            this.rnd = new Random();
        }

        public void Evolve()
        {
            List<Individual> newPop = new List<Individual>();
            for (int i = 0; i < popSize; i+= 2)
            {
                // select
                int[] p1 = Selection();
                int[] p2 = Selection();
                // cross
                int[][] offspring;
                if (this.rnd.NextDouble() < pcross)
                {
                    offspring = CrossoverOX(p1, p2);
                } else
                {
                    // no crossover
                    offspring = new int[][] { CopyArr(p1), CopyArr(p2) } ;
                }
                // mutate
                Individual m1 = new Individual(pop.tour, Mutate(offspring[0]));
                Individual m2 = new Individual(pop.tour, Mutate(offspring[1]));

                newPop.Add(m1);
                newPop.Add(m2);

            }
            // replace old population with new
            pop.population = newPop;
            pop.generation += 1;
            //Console.WriteLine(pop.ToString());
        }

        //  GenerationFitnessScores returns min, max and average adaptation scores for population at current state.
        public double[] GenerationFitnessScores()
        {
            return new double[] { pop.MinAdaptation(), pop.MaxAdaptation(), pop.AvgAdaptation() };
        }

        public double[] GenerationDistanceScores()
        {
            return new double[] { pop.MinDistance(), pop.MaxDistance(), pop.AvgDistance() };
        }

        public int[] Mutate(int[] original)
        {
            int[] seq = CopyArr(original);
            for (int i = 0; i < seq.Length; i++)
            {
                if (this.rnd.NextDouble() < pmutation)
                {
                    // get another random city index
                    int other = rnd.Next(seq.Length);

                    int tmp = seq[i];
                    seq[i] = seq[other];
                    seq[other] = tmp;
                }
            }
            return seq;
        }

        public int[] Selection()
        {
            double rand = this.rnd.NextDouble() * pop.FitnessSum();
            double partsum = 0.0;
            for (int i = 0; i < this.pop.population.Count; i++)
            {
                Individual cur = pop.population[i];
                partsum += cur.Fitness(pop.cmax);
                if (partsum >= rand)
                {
                    return CopyArr(cur.seq);
                }
            }
            // if none was found, return random
            return pop.population[rnd.Next(pop.population.Count - 1)].seq;
        }

        // crossover using modified method see pg.17 https://iccl.inf.tu-dresden.de/w/images/b/b7/GA_for_TSP.pdf
        public Individual[] CrossoverModified(Individual parent1, Individual parent2)
        {
            Tour tour = pop.tour;
            int crosspt = rnd.Next(1, parent1.seq.Length - 1);
            int[] child1 = parent1.seq.Take(crosspt).ToArray();
            Individual c1 = new Individual(tour, child1.Concat(AppendNoDupl(child1, parent2.seq)).ToArray());
            int[] child2 = parent2.seq.Take(crosspt).ToArray();
            Individual c2 = new Individual(tour, child2.Concat(AppendNoDupl(child2, parent1.seq)).ToArray());
            return new Individual[] { c1, c2 };
        }

        public int[][] CrossoverOX(int[] parent1, int[] parent2)
        {
            int[] crosspoints = TwoRandomCrosspoints(parent1.Length);       
            int[] offspring1 = OffspringSequenceOX(parent1, parent2, crosspoints);
            int[] offspring2 = OffspringSequenceOX(parent2, parent1, crosspoints);
            return new int[][] { offspring1, offspring2 };
        }

        private int[] OffspringSequenceOX(int[] p1seq, int[] p2seq, int[] crosspoints)
        {
            var p1seqCopied = CopyArr(p1seq);
            var p2seqCopied = CopyArr(p2seq);
            // take slice from middle e.g 5398|70|1462 => 70
            int[] middle = p1seqCopied.Skip(crosspoints[0])
                                .Take(crosspoints[1] - crosspoints[0]).ToArray();
            // sequence from another parent starting from 2nd cross point e.g. 6372|80|1954 => 1954637280
            int[] slice = p2seqCopied.Skip(crosspoints[1])
                               .Concat(p2seqCopied.Take(crosspoints[1])).ToArray();
            // result sequence starting from 2st cross point e.g. p1=5398|70|1462 p2=6372|80|1954 => c1=7019546328
            int[] seqUnordered = middle.Concat(AppendNoDupl(middle, slice)).ToArray();
            // sequence in correct order e.g. 7019546328 => 6328701954
            int[] seqOrdered = seqUnordered.Skip(p1seqCopied.Length - crosspoints[0])
                                           .Concat(seqUnordered.Take(p1seqCopied.Length - crosspoints[0])).ToArray();
            return seqOrdered;
        }

        // AppendNoDupl 
        private int[] AppendNoDupl(int[] taken, int[] from)
        {
            List<int> result = new List<int>();
            for (int i = 0; i < from.Length; i++)
            {
                if (taken.Contains(from[i]) == false)
                {
                    result.Add(from[i]); 
                }
            }
            return result.ToArray();
        }

        private int[] TwoRandomCrosspoints(int seqLen)
        {
            int crosspt1 = rnd.Next(1, seqLen - 1);
            int crosspt2;
            // crosspoint cannot be duplicates - choose 2nd one randomly until they are different
            while (true)
            {
                crosspt2 = rnd.Next(1, seqLen - 1);
                if (crosspt2 != crosspt1)
                {
                    break;
                }
            }
            return new int[] { crosspt1, crosspt2 }.OrderBy(x => x).ToArray();
        }

        private int[] CopyArr(int[] original)
        {
            int[] res = new int[original.Length];
            foreach (int i in original)
            {
                res[i] = original[i];
            }
            return res;
        }
    }
}
