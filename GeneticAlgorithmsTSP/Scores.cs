﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmsTSP
{
    public class Scores
    {

        private List<double> MinScores;
        private List<double> MaxScores;
        private List<double> AvgScores;

        public Scores()
        {
            this.MinScores = new List<double>();
            this.MaxScores = new List<double>();
            this.AvgScores = new List<double>();
        }

        public void AddMinMaxAvgScores(double[] scores)
        {
            this.MinScores.Add(scores[0]);
            this.MaxScores.Add(scores[1]);
            this.AvgScores.Add(scores[2]);
        }

        public List<double> GetMinScores()
        {
            return MinScores;
        }

        public List<double> GetMaxScores()
        {
            return MaxScores;
        }

        public List<double> GetAvgScores()
        {
            return AvgScores;
        }

        public double MaxScoreGlobal()
        {
            return MaxScores.Max();
        }

    }
}
