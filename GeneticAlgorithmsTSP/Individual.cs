﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmsTSP
{
    public class Individual
    {
        public Tour tour;
        public int[] seq;

        public Individual(Tour tour, int[] seq)
        {
            this.tour = tour;
            this.seq = seq;
        }

        public Individual(Tour tour, Random rnd)
        {
            this.tour = tour;
            this.seq = Enumerable.Range(0, tour.cities.Count).ToArray();
            this.seq.Shuffle(rnd);
        }

        // Fitness return individual fitness.
        // with roulette wheel selection, calculating fitness as 1 / distance gives not enough selection pressure.
        // to mitigate that effect, increase relative differences by powering.
        // this might cause trouble if distance between (0,1)
        public double Fitness(double cmax)
        {
            double distance = Distance();
            return Math.Pow(1 / distance, 5);
        }

        public double Distance()
        {
            double totalDistance = 0.0;
            for (int i = 1; i < seq.Length; i++)
            {
                totalDistance += this.tour.cities[seq[i]].distanceToCity(this.tour.cities[seq[i - 1]]);
            }
            // Distance between first and last node
            totalDistance += this.tour.cities[seq[0]].distanceToCity(this.tour.cities[seq[seq.Length - 1]]);
            return totalDistance;
        }

        public int GetCityX(int i)
        {
            return this.tour.cities[seq[i]].x;
        }

        public int GetCityY(int i)
        {
            return this.tour.cities[seq[i]].y;
        }

        public override string ToString()
        {
            string res = " ";
            foreach (int i in this.seq)
            {
                res += i.ToString() + " ";
            }
            return res;
        }
    }

    public static class Shuffler
    {
        public static void Shuffle<T>(this T[] array, Random rnd)
        {
            int n = array.Length;
            while (n > 1)
            {
                int k = rnd.Next(n);
                n--;
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
    }
}
