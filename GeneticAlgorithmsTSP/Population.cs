﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmsTSP
{
    public class Population
    {
        public int generation { get; set; }
        public double cmax;
        private Random rnd;
        public Tour tour;
        public List<Individual> population;

        public Population(Tour tour, int popCount)
        {
            this.rnd = new Random();
            this.generation = 0;
            this.tour = tour;
            population = new List<Individual>();
            for (int i = 0; i < popCount; i++)
            {
                population.Add(new Individual(this.tour, rnd));
            }
            this.cmax = AvgAdaptation();
        }

        public double FitnessSum()
        {
            double fitnessSum = 0.0;
            for (int i = 0; i < population.Count; i++)
            {
                fitnessSum += population[i].Fitness(cmax);
            }
            return fitnessSum;
        }
        

        public double MinAdaptation()
        {
            return population.Min(x => x.Fitness(cmax));
        }

        public double MaxAdaptation()
        {
            return population.Max(x => x.Fitness(cmax));
        }

        public double AvgAdaptation()
        {
            return population.Average(x => x.Fitness(cmax));
        }

        public double MinDistance()
        {
            return population.Min(x => x.Distance());
        }

        public double MaxDistance()
        {
            return population.Max(x => x.Distance());
        }

        public double AvgDistance()
        {
            return population.Average(x => x.Distance());
        }

        public Individual GetBestIndividual()
        {
            foreach( Individual i in population)
            {
                if (i.Fitness(cmax) == MaxAdaptation())
                {
                    return i;
                }
            }
            return null;
        }

        public Individual GetWorstIndividual()
        {
            foreach (Individual i in population)
            {
                if (i.Fitness(cmax) == MinAdaptation())
                {
                    return i;
                }
            }
            return null;
        }

        public override string ToString()
        {
            string resString = "Generation " + this.generation + ", avg.dist=" + Math.Round(AvgDistance(), 3) + "\n";
            foreach (Individual i in population)
            {
                resString += "(" + i.ToString() + "), fit=" + Math.Round(i.Distance(), 3).ToString() + "\n";
            }
            return resString;
        }
    }
}
