﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmsTSP
{
    public class Tour
    {
        public List<City> cities;

        public Tour()
        {
            cities = new List<City>();
        }

        // Adds cities to Tour from csv file
        public void AddFromCsv(string filename)
        {
            cities = File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filename))
                               .Skip(1)
                               .Select(v => Tour.FromCsv(v))
                               .ToList();
        }

        private static City FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            return new City(values[0], Convert.ToInt32(values[1]), Convert.ToInt32(values[2]));
        }

        public void AddCity(string name, int x, int y)
        {
            cities.Add(new City(name, x, y));
        }

        public void AddCity(City c)
        {
            cities.Add(c);
        }

        public override string ToString()
        {
            string resString = "";
            foreach (City c in cities)
            {
                resString += c.ToString() + ", ";
            }
            return resString;
        }
    }
}
