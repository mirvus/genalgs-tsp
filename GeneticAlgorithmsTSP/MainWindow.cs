﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization;

namespace GeneticAlgorithmsTSP
{
    public partial class MainWindowForm : Form
    {
        private String filePath;
        private BindingList<string> logItems = new BindingList<string>();

        public MainWindowForm()
        {
            InitializeComponent();
            this.filePath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\data\\cities.csv";
            this.textBoxFileSelect.Text = Path.GetFileName(this.filePath);

            listBoxLog.DataSource = logItems;
            logItems.Add(DateTime.Now.ToString("[h:mm:ss] ") + "Application started.");
        }

        public void RunSimulation(string csvFilePath, int maxGen, int popSize, double pcross, double pmutation)
        {
            Tour tour = new Tour();
            tour.AddFromCsv(csvFilePath);
            Scores scores = new Scores();

            GeneticAlgorithm simulation = new GeneticAlgorithm(tour, popSize, pcross, pmutation);
            for (int i = 0; i < maxGen; i++)
            {
                simulation.Evolve();
                scores.AddMinMaxAvgScores(simulation.GenerationDistanceScores());
            }

            Population lastPop = simulation.pop;
            Individual bestIndividual = lastPop.GetBestIndividual();
            Individual worstIndividual = lastPop.GetWorstIndividual();

            DrawTourMap(BestTourMap, "Best Tour", bestIndividual);
            DrawTourMap(WorstTourMap, "Worst Tour", worstIndividual);
            DrawAdaptationCharts(scores);
        }

        public void DrawTourMap(System.Windows.Forms.DataVisualization.Charting.Chart chartTarget, string name, Individual ind)
        {
            chartTarget.Series.Add(name);
            chartTarget.ChartAreas[0].AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartTarget.ChartAreas[0].AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.NotSet;
            chartTarget.Titles.Add(name);
            chartTarget.Titles[0].Text = name +  ", dist=" + Math.Round(ind.Distance(), 3);

            // draw points
            chartTarget.Series[name].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            foreach (City c in ind.tour.cities)
            {
                chartTarget.Series[name].Points.AddXY(c.x, c.y);
            }
            // draw lines connecting cities for best individual
            string nameBest = name + "best";
            chartTarget.Series.Add(nameBest);
            chartTarget.Series[nameBest].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            for (int i=0; i < ind.seq.Length; i++)
            {
                chartTarget.Series[nameBest].Points.AddXY(ind.GetCityX(i), ind.GetCityY(i));
            }
            // connect first point with last
            chartTarget.Series[nameBest].Points.AddXY(ind.GetCityX(0), ind.GetCityY(0));
        }

        public void DrawAdaptationCharts(Scores scores)
        {
            double MaxAxisY = scores.MaxScoreGlobal() * 1.1;
            

            // Averages chart
            List<double> avgScores = scores.GetAvgScores();
            string nameAvg = "avgAdaptation";
            AvgAdaptationChart.Series.Add(nameAvg);
            AvgAdaptationChart.Series[nameAvg].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            AvgAdaptationChart.Titles.Add(nameAvg);
            AvgAdaptationChart.Titles[0].Text = "Average fitness scores for generation";
            //AvgAdaptationChart.ChartAreas[0].AxisX.Interval = 10;
            AvgAdaptationChart.ChartAreas[0].AxisY.Maximum = MaxAxisY;
            for (int i = 0; i < avgScores.Count; i++)
            {
                AvgAdaptationChart.Series[nameAvg].Points.Add(avgScores[i]);
            }

            // Mins chart
            List<double> minScores = scores.GetMinScores();
            string nameMin = "minAdaptation";
            MinAdaptationChart.Series.Add(nameMin);
            MinAdaptationChart.Series[nameMin].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            MinAdaptationChart.Titles.Add(nameMin);
            MinAdaptationChart.Titles[0].Text = "Minimal fitness in generation";

            MinAdaptationChart.ChartAreas[0].AxisY.Maximum = MaxAxisY;
            for (int i = 0; i < minScores.Count; i++)
            {
                MinAdaptationChart.Series[nameMin].Points.Add(minScores[i]);
            }

            List<double> maxScores = scores.GetMaxScores();
            string nameMax = "maxAdaptation";
            MaxAdaptationChart.Series.Add(nameMax);
            MaxAdaptationChart.Series[nameMax].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            MaxAdaptationChart.Titles.Add(nameMax);
            MaxAdaptationChart.Titles[0].Text = "Maximal fitness in generation";
            MaxAdaptationChart.ChartAreas[0].AxisY.Maximum = MaxAxisY;
            for (int i = 0; i < maxScores.Count; i++)
            {
                MaxAdaptationChart.Series[nameMax].Points.Add(maxScores[i]);
            }
        }

        private void buttonRunSimulation_Click(object sender, EventArgs e)
        {
            clearCharts();

            string citiesCsvPath = this.filePath;
            int maxGen = Convert.ToInt32(numericUpDownMaxGenerations.Value);
            int popSize = Convert.ToInt32(numericUpDownPopCount.Value);
            double pcross = Convert.ToDouble(numericUpDownCrossProb.Value);
            double pmutation = Convert.ToDouble(numericUpDownMutationProb.Value);
            addSimulationLog(citiesCsvPath, maxGen, popSize, pcross, pmutation);
            RunSimulation(citiesCsvPath, maxGen, popSize, pcross, pmutation);
        }

        public void clearCharts()
        {
            BestTourMap.Series.Clear();
            BestTourMap.Titles.Clear();
            WorstTourMap.Series.Clear();
            WorstTourMap.Titles.Clear();
            AvgAdaptationChart.Series.Clear();
            AvgAdaptationChart.Titles.Clear();
            MinAdaptationChart.Series.Clear();
            MinAdaptationChart.Titles.Clear();
            MaxAdaptationChart.Series.Clear();
            MaxAdaptationChart.Titles.Clear();

        }

        private void buttonBrowseFile_Click(object sender, EventArgs e)
        {
            this.openFileDialog.InitialDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "//data";
            DialogResult result = this.openFileDialog.ShowDialog();
            // if a file is selected
            if (result == DialogResult.OK)
            {
                this.textBoxFileSelect.Text = Path.GetFileName(this.openFileDialog.FileName);
                this.filePath = openFileDialog.FileName;
            }
        }

        private void addSimulationLog(string csvFilePath, int maxGen, int popSize, double pcross, double pmutation)
        {
            logItems.Add(DateTime.Now.ToString("[h:mm:ss] ") + "New Simulation");
            logItems.Add("\t file: " + Path.GetFileName(csvFilePath));
            logItems.Add("\t maxgen: " + maxGen.ToString());
            logItems.Add("\t population: " + popSize.ToString());
            logItems.Add("\t pcross: " + pcross.ToString());
            logItems.Add("\t pmutation: " + pmutation.ToString());
            listBoxLog.TopIndex = listBoxLog.Items.Count - 1;
        }
    }
}
