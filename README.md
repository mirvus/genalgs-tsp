# GeneticAlgorithmsTSP
Windows Forms Application that solves travelling salesman problem using genetic algorithms.

Uses roulette wheel selection method and OX crossover.

## InputFiles (CSV format)
~~~
name,x,y
A,4,4
B,1,1
C,8,9
D,2,10
E,4,10
F,6,9
G,5,6
H,1,8
I,8,7
J,9,4
~~~

## Screenshot
![C# Program Screenshot](tsp_screenshot.png)

## References
[GA for TSP](https://iccl.inf.tu-dresden.de/w/images/b/b7/GA_for_TSP.pdf)